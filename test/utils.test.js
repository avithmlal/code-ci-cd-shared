import { expect } from 'chai';
import generateGreeting from '../utils.js';

describe('generateGreeting', () => {
  it('should return a greeting message with the given name', () => {
    const result = generateGreeting('John');
    expect(result).to.eql({ message: 'Hello Johns' });
  });

  it('should throw an error if name is not provided', () => {
    expect(() => generateGreeting()).to.throw('Name is required');
  });
});

